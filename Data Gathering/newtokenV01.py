import json
import requests
import re
import time
import os
from web3 import Web3
from bs4 import BeautifulSoup
import cloudscraper
import datetime
from telethon import TelegramClient, events, sync
import coinmarketcapapi


#node ethereum
NODE_URL = "http://xxxxx"


###
#Telethon Variable
###
api_id = xxx
api_hash = 'xxxx'
chatid = 'HoneypotIsBot'
phone = '+xxx'
SessionName = "telethon"


DBFILE = "tokenDB-1inch.txt"
DB_TOKEN = "uni-v2-tokenall.txt"
DB_EXCHANGE = "uni-v2-exchangeall.txt"
DB_LAST_NUMBER = "last.txt"
DB_DATA_TOKEN = "token-data.txt"
DB_RESCANTOKEN = "intermediate.txt"

DEVEL_MODE = False
if "http" in NODE_URL:
    w3 = Web3(Web3.HTTPProvider(NODE_URL))
elif "geth.ipc" in NODE_URL:
    w3 = Web3(Web3.IPCProvider(NODE_URL))
else:
    w3 = Web3(Web3.WebsocketProvider(NODE_URL))

exchangeFactorySC = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f"

ERC20_UNISWAP_EXCHANGEFACTORY_ABI = json.loads('[{"inputs":[{"internalType":"address","name":"_feeToSetter","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"token0","type":"address"},{"indexed":true,"internalType":"address","name":"token1","type":"address"},{"indexed":false,"internalType":"address","name":"pair","type":"address"},{"indexed":false,"internalType":"uint256","name":"","type":"uint256"}],"name":"PairCreated","type":"event"},{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"allPairs","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"allPairsLength","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"}],"name":"createPair","outputs":[{"internalType":"address","name":"pair","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"feeTo","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"feeToSetter","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"getPair","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_feeTo","type":"address"}],"name":"setFeeTo","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_feeToSetter","type":"address"}],"name":"setFeeToSetter","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"}]')
ERC20_TOKEN_ABI = json.loads('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}]')
UNISWAP_EXCHANGE_ABI = json.loads('[{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"},{"indexed":true,"internalType":"address","name":"to","type":"address"}],"name":"Burn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"}],"name":"Mint","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0In","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1In","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount0Out","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1Out","type":"uint256"},{"indexed":true,"internalType":"address","name":"to","type":"address"}],"name":"Swap","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint112","name":"reserve0","type":"uint112"},{"indexed":false,"internalType":"uint112","name":"reserve1","type":"uint112"}],"name":"Sync","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"constant":true,"inputs":[],"name":"DOMAIN_SEPARATOR","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"MINIMUM_LIQUIDITY","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"PERMIT_TYPEHASH","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"burn","outputs":[{"internalType":"uint256","name":"amount0","type":"uint256"},{"internalType":"uint256","name":"amount1","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"factory","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getReserves","outputs":[{"internalType":"uint112","name":"_reserve0","type":"uint112"},{"internalType":"uint112","name":"_reserve1","type":"uint112"},{"internalType":"uint32","name":"_blockTimestampLast","type":"uint32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_token0","type":"address"},{"internalType":"address","name":"_token1","type":"address"}],"name":"initialize","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"kLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"mint","outputs":[{"internalType":"uint256","name":"liquidity","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"nonces","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"permit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"price0CumulativeLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"price1CumulativeLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"skim","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"amount0Out","type":"uint256"},{"internalType":"uint256","name":"amount1Out","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"bytes","name":"data","type":"bytes"}],"name":"swap","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"sync","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"token0","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"token1","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"}]')

# API KEY for 3rd Party

BLOXY_API_KEY = "xxx"
ETHERSCAN_API_KEY = "xxx"
BEARER_TOKEN = "xxx%"

URL_BLOXY = "https://api.bloxy.info/token/token_info?token=_TOKEN_SC_&key=_KEY_&format=json"
URL_DISCORD = "xxx"
if DEVEL_MODE:
    URL_DISCORD = "xxx"


scraper = cloudscraper.create_scraper()

def toDict(dictToParse):
    # convert any 'AttributeDict' type found to 'dict'
    parsedDict = dict(dictToParse)
    for key, val in parsedDict.items():
        # check for nested dict structures to iterate through
        if  'dict' in str(type(val)).lower():
            parsedDict[key] = toDict(val)
        # convert 'HexBytes' type to 'str'
        elif 'HexBytes' in str(type(val)):
            parsedDict[key] = val.hex()
    return parsedDict

#Appends token information to the token database.
def appendTokenDB(inputan):
    f = open(DB_TOKEN, 'a')
    f.write(inputan)
    f.close()

#appends the inputan (exchange information) to the Exchange database file.
def appendExchangeDB(inputan):
    f = open(DB_EXCHANGE, 'a')
    f.write(inputan)
    f.close()

#Function to append token data to the database file "DB_DATA_TOKEN". It takes in a string "inputan" and writes it to the end of the file.
def appendDataTokenDB(inputan):
    f = open(DB_DATA_TOKEN, 'a')
    f.write(inputan)
    f.close()

#Appends the given input to the end of the re-scan token database file.
def appendReScanTokenDB(inputan):
    f = open(DB_RESCANTOKEN, 'a')
    f.write(inputan)
    f.close()

#Check if the token contract address exists in the database
def cekTokenDB(sc_address):
    sc_address = sc_address.lower()[2:]
    f = open(DB_TOKEN, "r")
    contents = f.read()
    f.close()
    raw = re.findall(sc_address+":.+\:.+", contents)
    if raw:
        return True
    else:
        return False

#Returns the last processed pair number from the database.
def cekLastNumberFromDB():
    f = open(DB_LAST_NUMBER, "r")
    contents = f.read()
    f.close()
    return int(contents)

#Set the last number of token pairs processed in the database.
def setLastNumberFromDB(last_number):
    f = open(DB_LAST_NUMBER, 'w')
    f.write(last_number)
    f.close()

#Check if exchange already exists in database
def cekExchangeDB(sc_address):
    sc_address = sc_address.lower()
    f = open(DB_TOKEN, "r")
    contents = f.read()
    f.close()
    raw = re.findall(".+\:"+sc_address, contents)
    if raw:
        return True
    else:
        return False

#getTokenName - retrieves token information from the database given a smart contract address. Returns symbol, smart contract address, and decimals as a dictionary.
def getTokenName(sc_address):
    sc_address = sc_address.lower()[2:]
    f = open(DBFILE, "r")
    contents = f.read()
    f.close()
    data = {}
    raw = re.findall(sc_address+":.+\:.+", contents)
    if raw:
            retraw = raw[0].split(":")
            data["symbol"] = retraw[1]
            data["sc"] = "0x"+retraw[0]
            data["decimals"] = retraw[2]
    return data

#This function retrieves the smart contract (SC) address and decimal of a token given its symbol.
def getTokenSC(koin):
    koin = koin.upper()
    f = open(DBFILE, "r")
    contents = f.read()
    f.close()
    data = {}
    raw = re.findall(".+\:"+koin+"+\:.+", contents)
    if raw:
            retraw = raw[0].split(":")
            data["symbol"] = retraw[1]
            data["sc"] = "0x"+retraw[0]
            data["decimals"] = retraw[2]
    return data

#Get the buying price of a token on Uniswap using the token_sc address and the amount of tokens. The number of decimals is set to 18 by default, but can be changed. The function returns the price in ETH.
def getUniswapHargaBuy(token_sc,decimals = 18,amount = float(1)):
    decimals = int(decimals)
    a = uniswap_get_eth_token_input_price(token_sc,int(float(amount)*10**18))
    try:
        b = format(float( float(amount) / (a/10**decimals)),".8f")
    except ZeroDivisionError:
        b = float(0)
    return b

#Get public price for ETH to Token trade with an exact input.
def uniswap_get_eth_token_input_price(exchange_sc,qty = 1):
     """Public price for ETH to Token trades with an exact input."""
     global UNISWAP_EXCHANGE_ABI
     amount = 0
     try:
         uniswapExchangeContract = w3.eth.contract(address=Web3.toChecksumAddress(exchange_sc),abi=UNISWAP_EXCHANGE_ABI)
     except:
         pass
     return amount

#getTokenInformation retrieves information about an ERC20 token contract, including its symbol, decimals, and name. If the information cannot be retrieved, it returns "null".
def getTokenInformation(token_sc):
    tokenContract = w3.eth.contract(address=Web3.toChecksumAddress(token_sc.lower()),
                                    abi=ERC20_TOKEN_ABI)
    token = {}
    try:
        symbol = tokenContract.functions.symbol().call()
        decimals = tokenContract.functions.decimals().call()
        name = tokenContract.functions.name().call()
        token['symbol'] = symbol
        token['decimals'] = decimals
        token['name'] = name
    except:
        token['symbol'] = 'null'
        token['decimals'] = 'null'
        token['name'] = 'null'
    return token

def getTokenInformationEtherscan(token_sc):
    token_sc = token_sc.lower()
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    URL =  "https://etherscan.io/token/"+token_sc
    r = requests.get(URL, proxies=proxies, headers=headers, timeout=20)
    data = r.text
    soup = BeautifulSoup(data, 'html.parser')
    decimals = soup.find_all('div', class_='col-md-8')[4].get_text()
    token_name = soup.find_all('span', class_='text-secondary small')[0].get_text()
    token_symbol1 = soup.find_all('div', class_='col-md-8 font-weight-medium')[0].get_text()
    token_symbol2 = token_symbol1.split(" ", 1)[1].rsplit(" ", 1)[0]
    decimals = int(decimals)
    token = {}
    token['symbol'] = token_symbol2
    token['decimals'] = decimals
    token['name'] = token_name
    return token

#A function that retrieves information about a token from Etherscan using the token's address
def getTokenInformationBloxy(token_sc):
    token_sc = token_sc.lower()
    URL = URL_BLOXY.replace("_TOKEN_SC_",token_sc)
    URL = URL.replace("_KEY_",BLOXY_API_KEY)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
    r = requests.get(URL, headers=headers, timeout=20)
    data = json.loads(r.text)
    token = {}
    try:
        token['symbol'] = data[0]['symbol']
        token['decimals'] = int(data[0]['decimals'])
        token['name'] = data[0]['name']
    except:
        token['symbol'] = 'null'
        token['decimals'] = 'null'
        token['name'] = 'null'
    return token

def kirimDiscord(channel,msg,username = "uni-v2-scanner",tts=False):
    return
    url = channel
    msg = msg.replace("*","**")
    if tts:
        data = {'username': username, 'content': msg, 'tts': True}
    else:
        data = {'username': username, 'content': msg}
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url, data=json.dumps(data), headers=headers)


def getBalanceWeth(token_sc):
    # This variable stores the address of the WETH smart contract
    weth_sc = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2"
    # This creates an instance of the ERC20 token contract using the WETH contract address and its ABI
    erc20 = w3.eth.contract(address=Web3.toChecksumAddress(weth_sc), abi=ERC20_TOKEN_ABI)
    # This calls the balanceOf() function of the ERC20 contract to get the balance of the specified token
    TokenAvailablebalance = erc20.functions.balanceOf(Web3.toChecksumAddress(token_sc.lower())).call()
    # This initializes the weth_amount variable as 0 and converts the TokenAvailablebalance to a float and divides it by 10^18
    weth_amount = float(0)
    if int(TokenAvailablebalance) > int(0):
        weth_amount = float(TokenAvailablebalance) / float(10**18)
    # This returns the weth_amount
    return weth_amount


#This function retrieves information about the creator of a given token smart contract (token_sc) by sending a GET request to the Etherscan API
def TokenCreatorAndTRX(token_sc):
    token_sc = token_sc.lower()
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
    URL =  "https://api.etherscan.io/api?module=contract&action=getcontractcreation&contractaddresses="+token_sc+"&apikey="+ETHERSCAN_API_KEY
    r = requests.get(URL, proxies=proxies, headers=headers, timeout=20)
    data = r.json()
    creator = {}
    creator['wallet'] = data['result'][0]['contractCreator']
    creator['txhash'] = data['result'][0]['txHash']
    return creator

def GetTrxDetails(txhash):
    trxdetails = toDict(w3.eth.getTransaction(txhash))
    creator = {}
    creator['blocknumber'] = trxdetails['blockNumber']
    creator['nonce'] = trxdetails['nonce']
    return creator

def getWalletReceiverAndNonce(TxHash):
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    EthWalletRegex = re.compile('^0x[a-fA-F0-9]{40}$')
    
    NonceRegexOne = re.compile('^[0-9]*$')

    URL2 =  "https://etherscan.io/tx/"+TxHash
    r2 = requests.get(URL2, proxies=proxies, headers=headers, timeout=30)
    data2 = r2.text
    soup2 = BeautifulSoup(data2, 'html.parser')

    ###########
    ## Find first wallet received the token
    ###########

    try:
        AddressReceiver1 = soup2.find_all('span', class_="d-block")[1].get('title')
        if re.match(EthWalletRegex, AddressReceiver1):
            AddressReceiver = AddressReceiver1
        else:
            AddressReceiver2 = soup2.find_all('span', class_='hash-tag text-truncate hash-tag-custom-to tooltip-address')[0].get('title')
            AddressReceiver2 = AddressReceiver2.split()
            AddressReceiver = AddressReceiver2[-1].replace('(', '').replace(')', '')
    except:
        AddressReceiver = "N/A"

    ###########
    ## Find BLOCK
    ###########
    BlockNumber = (soup2.find_all('span', class_='d-flex align-items-center gap-1')[0].get_text())

    ###########
    ## Find Wallet Nonce
    ###########
    walletNonce = (soup2.find_all('span', class_='badge bg-secondary bg-opacity-10 border border-secondary border-opacity-25 text-dark fw-medium text-start text-wrap py-1.5 px-2')[4].get_text()).split()[1]
    if re.match(NonceRegexOne, walletNonce):
        walletNonce = walletNonce
    else:
        walletNonceTwo = (soup2.find_all('span', class_='u-label u-label--value u-label--secondary rounded mr-1 mb-1')[0].get_text()).split()[1]
        walletNonce = walletNonceTwo


    creator = {}
    creator['receiver'] = AddressReceiver
    creator['blocknumber'] = BlockNumber
    creator['nonce'] = walletNonce

    return creator


def currentBlock():  # Defines a function called "currentBlock"
    latest = w3.eth.blockNumber  # Assigns the latest block number to a variable called "latest"
    return latest  # Returns the value of "latest" when the function is called


def blockTimeStamp(blockNumber):     # Defines a function named blockTimeStamp that takes a blockNumber argument
    block = w3.eth.getBlock(blockNumber).timestamp    # Calls the web3.py function `getBlock` with the `blockNumber` argument and gets the `timestamp` value from the returned block object, then assigns it to a variable named `block`
    return block      # Returns the value of the `block` variable as the output of the function


def holdersByAPI(token_sc):  # Defines a function named holdersByAPI which takes a parameter token_sc
    token_sc = token_sc.lower()  # Convert token_sc to lowercase and assign it back to the same variable
    proxies = {  # Create a dictionary named proxies
        "http": "http://167.71.200.126:8447",  # Assigns a value to the key "http"
        "https": "http://167.71.200.126:8447"  # Assigns a value to the key "https"
    }
    headers = {  # Create a dictionary named headers
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'  # Assigns a value to the key 'User-Agent'
    }

    URL =  "https://api.holders.at/holders?network=ethereum&collection="+token_sc+"&block="+str(currentBlock())  # Concatenates strings to form a URL and assigns it to a variable named URL
    while True :  # Starts a loop that will run indefinitely until it is broken
        try:  # Start a try block
            r = requests.get(URL, proxies=proxies, headers=headers, timeout=30)  # Send a GET request to the URL with the proxies and headers specified, and assign the response to a variable named r
            data = r.json()  # Extract the JSON data from the response and assign it to a variable named data
            data = len(data)  # Get the length of the data and assign it back to the same variable
            return data  # Exit the function and return the value of data
        except requests.exceptions.ReadTimeout:  # If there is a ReadTimeout exception, start an except block
            data = 'N/A'  # Assign the string 'N/A' to a variable named data
            return data  # Exit the function and return the value of data

def cekRescanDB(exchange_sc):  # Defines a function named cekRescanDB which takes a parameter exchange_sc
    exchange_sc = exchange_sc.lower()  # Convert exchange_sc to lowercase and assign it back to the same variable
    f = open(DB_RESCANTOKEN, "r")  # Open a file named DB_RESCANTOKEN in read-only mode and assign it to a variable named f
    contents = f.read()  # Read the contents of the file and assign it to a variable named contents
    f.close()  # Close the file
    raw = re.findall(".+\:"+exchange_sc, contents)  # Use a regular expression to search for a pattern and assign the result to a variable named raw
    if raw:  # If raw is not empty, start an if block
        return True  # Exit the function and return the value True
    else:  # If raw is empty, start an else block
        return False  # Exit the function and return the value False


client = TelegramClient('session_name', api_id, api_hash)

client.connect()


def send(token_sc):
    client.send_message('HoneypotIsBot', '/honeypot '+token_sc)  # Send a message to the 'HoneypotIsBot' with the given token_sc appended to it.
    time.sleep(3)  # Wait for 3 seconds.

def honeypot():
    smileyok = re.compile('✅')  # Create a regular expression object to match the unicode character '✅'
    honeypotinfo = {}  # Initialize an empty dictionary for honeypot information.
    data = []  # Initialize an empty list for data.

    ############
    # Part to catch the return message from tele
    ############

    for message in client.get_messages(chatid, limit=1):  # Get the latest message from a chat with ID 'chatid'.
        pesan = message.message.split()  # Split the message into a list of words.
        data.append(pesan)  # Append the list of words to the data list.
    if re.match(smileyok, data[0][0]):  # Check if the first word of the first message matches the smileyok regex.
        honeypotinfo['honeypot'] = 'NO'  # Set honeypotinfo['honeypot'] to 'NO'.
        honeypotinfo['selltax'] = data[0][-4]  # Set honeypotinfo['selltax'] to the fourth last word of the message.
        honeypotinfo['buytax'] = re.findall('[0-9]+.[0-9]+%',data[0][-7])  # Set honeypotinfo['buytax'] to a list of percentages found in the seventh last word of the message.
    else:
        honeypotinfo['honeypot'] = 'YES'  # Set honeypotinfo['honeypot'] to 'YES'.
        honeypotinfo['selltax'] = 'N/A'  # Set honeypotinfo['selltax'] to 'N/A'.
        honeypotinfo['buytax'] = ['N/A']  # Set honeypotinfo['buytax'] to a list with 'N/A' as its only element.
    return honeypotinfo  # Return the honeypot information dictionary.

def TokenRenounce(token_sc): # Define a function named TokenRenounce that takes one argument called token_sc
    contract_abi = '''[ # Assign a string containing JSON formatted ABI to a variable called contract_abi
        {
        "constant": true,
        "inputs": [],
        "name": "owner",
        "outputs": [
            {
            "name": "",
            "type": "address"
            }
        ],
        "payable": false,
        "type": "function"
        },
        {
        "inputs": [],
        "payable": false,
        "type": "constructor"
        }
    ]'''
    contract_address = Web3.toChecksumAddress(token_sc) # Convert the token_sc argument to a checksummed Ethereum address and assign it to a variable called contract_address

    # Get the contract instance
    contract = w3.eth.contract(address=contract_address, abi=contract_abi) # Create a contract object using the contract_address and contract_abi

    try:
        # Call the function that returns the owner address
        contract_owner = contract.functions.owner().call() # Call the 'owner' function on the contract and assign the returned value to a variable called contract_owner

        # Check if the returned owner address is the null address
        if contract_owner == '0x0000000000000000000000000000000000000000': # Check if the contract_owner variable equals the null address
            data = "renounce" # If so, assign the string "renounce" to a variable called data
            return data # Return the data variable

        else:
            # Print the owner address
            data = contract_owner # If not, assign the contract_owner value to a variable called data
            return data # Return the data variable

    except:
        data = "error" # If an exception occurs during execution of the try block, assign the string "error" to a variable called data
        return data # Return the data variable


# Function to search recent tweets containing the given token
def search_twitter(token_sc):
    # Define the tweet fields to be fetched
    tweet_fields = "tweet.fields=text,author_id,created_at"
    # Define the headers for the API request with authorization token
    headers = {"Authorization": "Bearer {}".format(BEARER_TOKEN)}

    # Create the URL for API request with query and tweet fields
    url = "https://api.twitter.com/2/tweets/search/recent?query={}&{}".format(
        token_sc, tweet_fields  # Remark: format the token and tweet fields into the URL
    )
    # Send the GET request to the API and store the response
    response = requests.request("GET", url, headers=headers)

    # Raise an exception if the response code is not 200 OK
    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    # Convert the response to JSON format and extract the result count
    json_response = response.json()
    result_count = json_response["meta"]["result_count"]

    # Return the result count
    return result_count

# Function to check if the given token is supported on Ethereum platform
def CoinGecko(token_sc):
    # Convert the token to lowercase for comparison
    token_sc = token_sc.lower()
    # Define the proxies to be used for the API request
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    # Define the headers for the API request with user-agent information
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    # Create the URL for API request to fetch the list of coins with platform info
    URL =  "https://api.coingecko.com/api/v3/coins/list?include_platform=true"
    # Send the GET request to the API with proxies, headers and timeout
    r = requests.get(URL, proxies=proxies, headers=headers, timeout=20)
    # Convert the response to JSON format
    data = r.json()

    # Iterate over the list of coins and check if the token is supported on Ethereum platform
    for item in data:
        if "platforms" in item and "ethereum" in item["platforms"]:
            if item["platforms"]["ethereum"] == token_sc:
                # Return "Yes" if the token is supported on Ethereum platform
                return "Yes"
    # Return "No" if the token is not supported on Ethereum platform
    return "No"


def cmc(token_sc, token_symbol):
    # Set the environment variable
    os.environ['CMC-Key'] = "xxxx"  # Set the CMC API key as an environment variable

    # Retrieve the environment variable
    api_key = os.environ.get('CMC-Key')  # Retrieve the CMC API key from the environment variable

    symbol = token_symbol  # Assign the token symbol to a variable
    token_sc = token_sc  # Assign the token smart contract address to a variable

    cmc_client = coinmarketcapapi.CoinMarketCapAPI(api_key)  # Create a CMC API client object
    
    try:
        response = cmc_client.cryptocurrency_info(symbol=symbol)  # Make a request to the CMC API to retrieve cryptocurrency information
        findsc = response.data[symbol][0]['platform']['token_address']  # Extract the token smart contract address from the API response
        if token_sc == findsc:  # Compare the extracted token smart contract address with the provided token smart contract address
            data = "Yes"  # Assign the string "Yes" to the data variable if the token smart contract addresses match
            return data  # Return the data variable
        else:
            data = "No"  # Assign the string "No" to the data variable if the token smart contract addresses do not match
            return data  # Return the data variable
    except Exception as e:  # Handle any exceptions that occur while making the API request
        data = "No"  # Assign the string "No" to the data variable if an exception occurs
        return data  # Return the data variable


def do_scan():
    erc20 = w3.eth.contract(address=Web3.toChecksumAddress(exchangeFactorySC.lower()), abi=ERC20_UNISWAP_EXCHANGEFACTORY_ABI)
    exchange_count = erc20.functions.allPairsLength().call()

    print(currentBlock())
    jumlah = 0
    last_from_db = cekLastNumberFromDB() - 1
    for data in range(last_from_db,exchange_count):
        pair_sc = w3.eth.contract(address=Web3.toChecksumAddress(exchangeFactorySC.lower()),
                                abi=ERC20_UNISWAP_EXCHANGEFACTORY_ABI)
        exchange_sc = erc20.functions.allPairs(data).call()
        exchange_sc_contract = w3.eth.contract(address=Web3.toChecksumAddress(exchange_sc.lower()),
                                               abi=UNISWAP_EXCHANGE_ABI)

        reserves = exchange_sc_contract.functions.getReserves().call()

        token0 = exchange_sc_contract.functions.token0().call()
        token0_symbol = getTokenInformation(token0)['symbol']


        try:
            if token0_symbol == "null":
                token0_symbol = getTokenName(token0)['symbol']
        except:
            token0_symbol = "null"
 
        token1 = exchange_sc_contract.functions.token1().call()
        token1_symbol = getTokenInformation(token1)['symbol']


        try:
            if token1_symbol == "null":
                token1_symbol = getTokenName(token1)['symbol']
        except:
            token1_symbol = "null"

        ####
        ## CHECK WETH IF NOT ZERO
        ####
        if reserves[0] == 0:
            token_exchangeRescanDB = cekRescanDB(exchange_sc)
            if token_exchangeRescanDB == True :
                continue
            input_EmptyPool = str(data)+":"+exchange_sc.lower()+"\n"
            appendReScanTokenDB(input_EmptyPool)
            continue

        if token0_symbol.upper() == "WETH":
            token_sc = token1
            token_symbol = token1_symbol.upper()
        elif token1_symbol.upper() == "WETH":
            token_sc = token0
            token_symbol = token0_symbol.upper()
        else:
            continue
        decimals = getTokenInformation(token_sc)['decimals']
        try:
            if decimals == "null":
                decimals = getTokenName(token_sc)['decimals']
        except:
            decimals = "null"
        token_name = getTokenInformation(token_sc)['name']

        if decimals == "null" or token_symbol.upper() == "NULL" or token_name.upper()  == "NULL":
            token_info_bloxy =  getTokenInformationBloxy(token_sc)
            decimals = token_info_bloxy['decimals']
            token_symbol = token_info_bloxy['symbol']
            token_symbol = token_symbol.strip()
            token_symbol = token_symbol.upper()
            token_name = token_info_bloxy['name']
            token_name = token_name.strip()
            token_name = token_name.upper()
        weth_balance = getBalanceWeth(exchange_sc)
        weth_balance = format(float(weth_balance),".2f")

        if decimals != "null" or token_symbol != "NULL" :
            if token_symbol == "":
                continue

            token_db = cekTokenDB(token_sc)
            token_exchange = cekExchangeDB(exchange_sc)
            
            if token_db == True or token_exchange == True :
                continue
            
            try:
                CreatorAndTrx = TokenCreatorAndTRX(token_sc)
            except:
                continue

            try:
                FirstWalletBLKNumberAndNonce = getWalletReceiverAndNonce(CreatorAndTrx['txhash'])
            except:
                continue


            Holders = str(holdersByAPI(token_sc))

            TimeDifference = blockTimeStamp(currentBlock()) - blockTimeStamp(int(FirstWalletBLKNumberAndNonce['blocknumber']))
            humanTime = str(datetime.timedelta(seconds=TimeDifference))

            send(token_sc)
            honeyinfo = honeypot()

            info = "Uniswap V2 New Token\n"
            info += "symbol : "+token_symbol+"\n"
            info += "name : " + token_name + "\n"
            info += "Token SC : "+str(token_sc)+"\n"
            info += "Exchange SC : "+str(exchange_sc)+"\n"
            info += "Weth Balance: " + str(weth_balance) + " WETH \n"
            info += str(time.ctime())+"\n"
            info += "----"
            input_tokenDB = token_sc[2:].lower()+":"+token_symbol+":"+str(decimals)+"\n"
            input_exchangeDB = token_symbol+":"+exchange_sc.lower()+"\n"
            input_datatokenDB = str(data)+":"+token_sc.lower()+":"+token_symbol+":"+token_name+":"+str(weth_balance)+":"+exchange_sc.lower()+":"+CreatorAndTrx['wallet']+":"+CreatorAndTrx['txhash']+":"\
                +FirstWalletBLKNumberAndNonce['receiver']+":"+str(FirstWalletBLKNumberAndNonce['blocknumber'])+":"+str(currentBlock())+":"+str(TimeDifference)+":["+str(humanTime)+"]:"+str(FirstWalletBLKNumberAndNonce['nonce'])\
                    +":"+Holders+":"+str(honeyinfo['honeypot'])+":"+str(honeyinfo['buytax'][0])+":"+str(honeyinfo['selltax'])+":"+TokenRenounce(token_sc)+":"+str(search_twitter(token_sc))+":"+CoinGecko(token_sc)+":"+cmc(token_sc, token_symbol)+"\n"

            print(input_datatokenDB)
            appendTokenDB(input_tokenDB)
            appendExchangeDB(input_exchangeDB)
            appendDataTokenDB(input_datatokenDB)

            setLastNumberFromDB(str(data))

            kirimDiscord(URL_DISCORD,info)
            URL_CUSTOM = "xxx"
            if float(weth_balance) > float(500):
                kirimDiscord(URL_CUSTOM,info)
        time.sleep(1)


while True:
    print("start ---- "+str(time.ctime()))
    do_scan()
    print("finish ---- "+str(time.ctime()))
    time.sleep(20)

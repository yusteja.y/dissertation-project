from asyncore import loop
import json
import requests
import re
import time
import os
from web3 import Web3
from bs4 import BeautifulSoup
import cloudscraper
import datetime
from telethon import TelegramClient, events, sync
import coinmarketcapapi

#NODE_URL = "https://mainnet.infura.io/v3/c1b7868c46f0487a8df80535f9ce3b9e"
#NODE_URL = "https://black-empty-flower.discover.quiknode.pro/acfaf6101c1d34735e29cb99853c02dca06d8050/"
#NODE_URL = "http://51.79.230.134:8545"
NODE_URL = "http://141.95.35.66:8545"


DBFILE = "tokenDB-1inch.txt"
DB_TOKEN = "uni-v2-tokenall.txt"
DB_EXCHANGE = "uni-v2-exchangeall.txt"
DB_LAST_NUMBER = "last.txt"
DB_DATA_TOKEN = "token-data.txt"
DB_RESCANTOKEN = "rescan.txt"
DB_INTERMEDIATE = "intermediate.txt"

DEVEL_MODE = False
if "http" in NODE_URL:
    w3 = Web3(Web3.HTTPProvider(NODE_URL))
elif "geth.ipc" in NODE_URL:
    w3 = Web3(Web3.IPCProvider(NODE_URL))
else:
    w3 = Web3(Web3.WebsocketProvider(NODE_URL))

exchangeFactorySC = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f"

ERC20_UNISWAP_EXCHANGEFACTORY_ABI = json.loads('[{"inputs":[{"internalType":"address","name":"_feeToSetter","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"token0","type":"address"},{"indexed":true,"internalType":"address","name":"token1","type":"address"},{"indexed":false,"internalType":"address","name":"pair","type":"address"},{"indexed":false,"internalType":"uint256","name":"","type":"uint256"}],"name":"PairCreated","type":"event"},{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"allPairs","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"allPairsLength","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"}],"name":"createPair","outputs":[{"internalType":"address","name":"pair","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"feeTo","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"feeToSetter","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"getPair","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_feeTo","type":"address"}],"name":"setFeeTo","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_feeToSetter","type":"address"}],"name":"setFeeToSetter","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"}]')
ERC20_TOKEN_ABI = json.loads('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}]')
UNISWAP_EXCHANGE_ABI = json.loads('[{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"},{"indexed":true,"internalType":"address","name":"to","type":"address"}],"name":"Burn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"}],"name":"Mint","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0In","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1In","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount0Out","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1Out","type":"uint256"},{"indexed":true,"internalType":"address","name":"to","type":"address"}],"name":"Swap","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint112","name":"reserve0","type":"uint112"},{"indexed":false,"internalType":"uint112","name":"reserve1","type":"uint112"}],"name":"Sync","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"constant":true,"inputs":[],"name":"DOMAIN_SEPARATOR","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"MINIMUM_LIQUIDITY","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"PERMIT_TYPEHASH","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"burn","outputs":[{"internalType":"uint256","name":"amount0","type":"uint256"},{"internalType":"uint256","name":"amount1","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"factory","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getReserves","outputs":[{"internalType":"uint112","name":"_reserve0","type":"uint112"},{"internalType":"uint112","name":"_reserve1","type":"uint112"},{"internalType":"uint32","name":"_blockTimestampLast","type":"uint32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_token0","type":"address"},{"internalType":"address","name":"_token1","type":"address"}],"name":"initialize","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"kLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"mint","outputs":[{"internalType":"uint256","name":"liquidity","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"nonces","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"permit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"price0CumulativeLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"price1CumulativeLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"skim","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"amount0Out","type":"uint256"},{"internalType":"uint256","name":"amount1Out","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"bytes","name":"data","type":"bytes"}],"name":"swap","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"sync","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"token0","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"token1","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"}]')
BLOXY_API_KEY = "ACCzq61l8d4tJ"

ETHERSCAN_API_KEY = "CKFHTZ8GTBUIE81E2R5FPG3ZCQJA8AM969"

BEARER_TOKEN = "AAAAAAAAAAAAAAAAAAAAAMnahwEAAAAAxCVoTQlcvNiEuH8aDC8UOErb%2FyA%3DkAmdZTRtD4Ez6aPBwSDTBEclYuFw9gm4GFbKPEmfYd0InNJ84P"

URL_BLOXY = "https://api.bloxy.info/token/token_info?token=_TOKEN_SC_&key=_KEY_&format=json"
URL_DISCORD = "https://discord.com/api/webhooks/719100769760051250/4VUvpRvjq_2ki5xGFIasSLrp_-ksBgD7se9IECDZg9mzI6IvTJh1SX4Cjz-vNjPQU1bK"
if DEVEL_MODE:
    URL_DISCORD = "https://discord.com/api/webhooks/719101985651163156/gN7AjjF8f0h3KE_dfnYfGEmZQ6TB0P6PdrgWwV_L0t6Ffh-vcvLIkjI1L-KgdM4pHKHP"


scraper = cloudscraper.create_scraper()

def toDict(dictToParse):
    # convert any 'AttributeDict' type found to 'dict'
    parsedDict = dict(dictToParse)
    for key, val in parsedDict.items():
        # check for nested dict structures to iterate through
        if  'dict' in str(type(val)).lower():
            parsedDict[key] = toDict(val)
        # convert 'HexBytes' type to 'str'
        elif 'HexBytes' in str(type(val)):
            parsedDict[key] = val.hex()
    return parsedDict


def appendTokenDB(inputan):
    f = open(DB_TOKEN, 'a')
    f.write(inputan)
    f.close()

def appendExchangeDB(inputan):
    f = open(DB_EXCHANGE, 'a')
    f.write(inputan)
    f.close()

def appendDataTokenDB(inputan):
    f = open(DB_DATA_TOKEN, 'a')
    f.write(inputan)
    f.close()

def appendReScanTokenDB(inputan):
    f = open(DB_RESCANTOKEN, 'a')
    f.write(inputan)
    f.close()

def cekTokenDB(sc_address):
    sc_address = sc_address.lower()[2:]
    f = open(DB_TOKEN, "r")
    contents = f.read()
    f.close()
    raw = re.findall(sc_address+":.+\:.+", contents)
    if raw:
        return True
    else:
        return False

def cekLastNumberFromDB():
    f = open(DB_LAST_NUMBER, "r")
    contents = f.read()
    f.close()
    return int(contents)

def setLastNumberFromDB(last_number):
    f = open(DB_LAST_NUMBER, 'w')
    f.write(last_number)
    f.close()


def cekExchangeDB(sc_address):
    sc_address = sc_address.lower()
    f = open(DB_TOKEN, "r")
    contents = f.read()
    f.close()
    raw = re.findall(".+\:"+sc_address, contents)
    if raw:
        return True
    else:
        return False

def getTokenName(sc_address):
    sc_address = sc_address.lower()[2:]
    f = open(DBFILE, "r")
    contents = f.read()
    f.close()
    data = {}
    raw = re.findall(sc_address+":.+\:.+", contents)
    if raw:
            retraw = raw[0].split(":")
            data["symbol"] = retraw[1]
            data["sc"] = "0x"+retraw[0]
            data["decimals"] = retraw[2]
    return data


def getTokenSC(koin):
    koin = koin.upper()
    f = open(DBFILE, "r")
    contents = f.read()
    f.close()
    data = {}
    raw = re.findall(".+\:"+koin+"+\:.+", contents)
    if raw:
            retraw = raw[0].split(":")
            data["symbol"] = retraw[1]
            data["sc"] = "0x"+retraw[0]
            data["decimals"] = retraw[2]
    return data

def getUniswapHargaBuy(token_sc,decimals = 18,amount = float(1)):
    decimals = int(decimals)
    a = uniswap_get_eth_token_input_price(token_sc,int(float(amount)*10**18))
    try:
        b = format(float( float(amount) / (a/10**decimals)),".8f")
    except ZeroDivisionError:
        b = float(0)
    return b


def uniswap_get_eth_token_input_price(exchange_sc,qty = 1):
     """Public price for ETH to Token trades with an exact input."""
     global UNISWAP_EXCHANGE_ABI
     amount = 0
     try:
         uniswapExchangeContract = w3.eth.contract(address=Web3.toChecksumAddress(exchange_sc),abi=UNISWAP_EXCHANGE_ABI)
##         amount = uniswapExchangeContract.functions.getEthToTokenInputPrice(qty).call()
     except:
         pass
     return amount

def getTokenInformation(token_sc):
    tokenContract = w3.eth.contract(address=Web3.toChecksumAddress(token_sc.lower()),
                                    abi=ERC20_TOKEN_ABI)
    token = {}
    try:
        symbol = tokenContract.functions.symbol().call()
        decimals = tokenContract.functions.decimals().call()
        name = tokenContract.functions.name().call()
        token['symbol'] = symbol
        token['decimals'] = decimals
        token['name'] = name
    except:
        token['symbol'] = 'null'
        token['decimals'] = 'null'
        token['name'] = 'null'
    return token

def getTokenInformationEtherscan(token_sc):
    token_sc = token_sc.lower()
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    URL =  "https://etherscan.io/token/"+token_sc
    r = requests.get(URL, proxies=proxies, headers=headers, timeout=20)
    data = r.text
    soup = BeautifulSoup(data, 'html.parser')
    #print(soup)
    decimals = soup.find_all('div', class_='col-md-8')[4].get_text()
    token_name = soup.find_all('span', class_='text-secondary small')[0].get_text()
    token_symbol1 = soup.find_all('div', class_='col-md-8 font-weight-medium')[0].get_text()
    token_symbol2 = token_symbol1.split(" ", 1)[1].rsplit(" ", 1)[0]
    #print(decimals)
    #print(token_name)
    #print(token_symbol2)
    decimals = int(decimals)
    token = {}
    token['symbol'] = token_symbol2
    token['decimals'] = decimals
    token['name'] = token_name
    #print(token_name,decimals)
    return token

def getTokenInformationBloxy(token_sc):
    token_sc = token_sc.lower()
    URL = URL_BLOXY.replace("_TOKEN_SC_",token_sc)
    URL = URL.replace("_KEY_",BLOXY_API_KEY)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
    r = requests.get(URL, headers=headers, timeout=20)
    data = json.loads(r.text)
    token = {}
    try:
        token['symbol'] = data[0]['symbol']
        token['decimals'] = int(data[0]['decimals'])
        token['name'] = data[0]['name']
    except:
        token['symbol'] = 'null'
        token['decimals'] = 'null'
        token['name'] = 'null'
    return token

def kirimDiscord(channel,msg,username = "uni-v2-scanner",tts=False):
    return
    url = channel
    msg = msg.replace("*","**")
    if tts:
        data = {'username': username, 'content': msg, 'tts': True}
    else:
        data = {'username': username, 'content': msg}
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url, data=json.dumps(data), headers=headers)


def getBalanceWeth(token_sc):
    weth_sc = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2"
    erc20 = w3.eth.contract(address=Web3.toChecksumAddress(weth_sc), abi=ERC20_TOKEN_ABI)
    TokenAvailablebalance = erc20.functions.balanceOf(Web3.toChecksumAddress(token_sc.lower())).call()
    weth_amount = float(0)
    if int(TokenAvailablebalance) > int(0):
        weth_amount = float(TokenAvailablebalance) / float(10**18)
    return weth_amount


def TokenCreatorAndTRX(token_sc):
    token_sc = token_sc.lower()
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
    URL =  "https://api.etherscan.io/api?module=contract&action=getcontractcreation&contractaddresses="+token_sc+"&apikey="+ETHERSCAN_API_KEY
    r = requests.get(URL, proxies=proxies, headers=headers, timeout=20)
    data = r.json()
    creator = {}
    creator['wallet'] = data['result'][0]['contractCreator']
    creator['txhash'] = data['result'][0]['txHash']
    return creator

def getWalletReceiverAndNonce(TxHash):
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    EthWalletRegex = re.compile('^0x[a-fA-F0-9]{40}$')
    
    NonceRegexOne = re.compile('^[0-9]*$')
    #NonceRegexTwo = re.compile('^[a-zA-Z][a-zA-Z]$')

    URL2 =  "https://etherscan.io/tx/"+TxHash
    #print(URL2)
    r2 = requests.get(URL2, proxies=proxies, headers=headers, timeout=30)
    #r2 = scraper.get(URL2, proxies=proxies, headers=headers, timeout=30)
    data2 = r2.text
    #print(data2)
    soup2 = BeautifulSoup(data2, 'html.parser')
    #print(soup2)

    ###########
    ## Find first wallet received the token
    ###########

    try:
        AddressReceiver1 = soup2.find_all('span', class_="d-block")[1].get('title')
        #print(AddressReceiver1)
        if re.match(EthWalletRegex, AddressReceiver1):
            #print('IF')
            AddressReceiver = AddressReceiver1
        else:
            #print('check')
            AddressReceiver2 = soup2.find_all('span', class_='hash-tag text-truncate hash-tag-custom-to tooltip-address')[0].get('title')
            AddressReceiver2 = AddressReceiver2.split()
            #print(AddressReceiver2[-1].replace('(', '').replace(')', ''))
            AddressReceiver = AddressReceiver2[-1].replace('(', '').replace(')', '')
        #print(AddressReceiver)
    except:
        AddressReceiver = "N/A"
    #print(AddressReceiver)

        ###########
    ## Find BLOCK
    ###########
    #BlockNumber = (soup2.find_all('div', class_='col-md-9')[2].get_text()).split()[0]
    BlockNumber = (soup2.find_all('span', class_='d-flex align-items-center gap-1')[0].get_text())
    #print(BlockNumber)

    ###########
    ## Find Wallet Nonce
    ###########
    #walletNonce = (soup2.find_all('span', class_='text-muted')[1].get_text()).split()[1]
    walletNonce = (soup2.find_all('span', class_='badge bg-secondary bg-opacity-10 border border-secondary border-opacity-25 text-dark fw-medium text-start text-wrap py-1.5 px-2')[4].get_text()).split()[1]
    #print(walletNonce)
    if re.match(NonceRegexOne, walletNonce):
        walletNonce = walletNonce
    else:
        walletNonceTwo = (soup2.find_all('span', class_='u-label u-label--value u-label--secondary rounded mr-1 mb-1')[0].get_text()).split()[1]
        walletNonce = walletNonceTwo
    #print(walletNonce)


    creator = {}
    creator['receiver'] = AddressReceiver
    creator['blocknumber'] = BlockNumber
    creator['nonce'] = walletNonce

    #time.sleep(20)
    return creator

def currentBlock():
    latest = w3.eth.blockNumber
    return latest

def blockTimeStamp(blockNumber):
    block = w3.eth.getBlock(blockNumber).timestamp
    return block

def holdersByAPI(token_sc):
    token_sc = token_sc.lower()
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    URL =  "https://api.holders.at/holders?network=ethereum&collection="+token_sc+"&block="+str(currentBlock())
    #print(URL)
    while True :
        try:
            r = requests.get(URL, proxies=proxies, headers=headers, timeout=30)
            data = r.json()
            data = len(data)
            return data
        except:
            data = 'N/A'
            return data
    #print(r)
    #print(data)
    
    

def CheckRescanToken():
    x = []
    with open(DB_RESCANTOKEN, "r") as a_file:
        for line in a_file:
            stripped_line = (line.strip()).split(":")[0]
            x.append(str(stripped_line))
            #print(x)
    return x

def ReadRescanFile():
    # input text file
    inputFile = DB_RESCANTOKEN
    # Opening the given file in read-only mode.
    with open(inputFile, 'r') as filedata:
    # Read the file lines using readlines()
        inputFilelines = filedata.readlines()
    filedata.close
    return inputFilelines

def WriteRescanFile(inputFilelines):
    inputFile = DB_RESCANTOKEN
    with open(inputFile, 'w') as filedata:
        for textline in inputFilelines:
            filedata.write(textline)
    #givenFile = open(inputFile,"r")
    #for line in givenFile:
    #    print(line)
    filedata.close

###
#Telethon Variable
###
api_id = 24441608
api_hash = '275f2340bfb058f04c49b071033a5cc1'
chatid = 'HoneypotIsBot'
phone = '+447385592975'
SessionName = "telethon"

client = TelegramClient('rescan', api_id, api_hash)
#client = TelegramClient('session_name', api_id, api_hash)
client.start()
client.connect()


def send(token_sc):
    
    #client.start()
    
    client.send_message('HoneypotIsBot', '/honeypot '+token_sc)
    #print(token_sc)
    #client.disconnect()
    time.sleep(3)

def honeypot():
    #EthWalletRegex = re.compile('^0x[a-fA-F0-9]{40}$')
    smileyok = re.compile('✅')
    #smileynot = re.compile('❌')
    honeypotinfo = {}
    data = []
    #time.sleep(10)
    ############
    # Part to catch the return message from tele
    ############

    
    #client.start()
    #client.connect()

    for message in client.get_messages(chatid, limit=1):
        pesan = message.message.split()
        data.append(pesan)
        #print(message.message)
    if re.match(smileyok, data[0][0]):
        honeypotinfo['honeypot'] = 'NO'
        honeypotinfo['selltax'] = data[0][-4]
        honeypotinfo['buytax'] = re.findall('[0-9]+.[0-9]+%',data[0][-7])
    else:
        honeypotinfo['honeypot'] = 'YES'
        honeypotinfo['selltax'] = 'N/A'
        honeypotinfo['buytax'] = ['N/A']
    #client.disconnect()
    return honeypotinfo



def TokenRenounce(token_sc):
    contract_abi = '''[
        {
        "constant": true,
        "inputs": [],
        "name": "owner",
        "outputs": [
            {
            "name": "",
            "type": "address"
            }
        ],
        "payable": false,
        "type": "function"
        },
        {
        "inputs": [],
        "payable": false,
        "type": "constructor"
        }
    ]'''
    contract_address = Web3.toChecksumAddress(token_sc)

    # Get the contract instance
    contract = w3.eth.contract(address=contract_address, abi=contract_abi)

    try:
        # Call the function that returns the owner address
        contract_owner = contract.functions.owner().call()
        # Check if the returned owner address is the null address
        if contract_owner == '0x0000000000000000000000000000000000000000':
            data = "renounce"
            return data
        else:
            # Print the owner address
            data = contract_owner
            return data

    except:
        data = "error"
        return data


def search_twitter(token_sc):
    tweet_fields = "tweet.fields=text,author_id,created_at"
    headers = {"Authorization": "Bearer {}".format(BEARER_TOKEN)}

    url = "https://api.twitter.com/2/tweets/search/recent?query={}&{}".format(
        token_sc, tweet_fields
    )
    response = requests.request("GET", url, headers=headers)

    #print(response.status_code)
    #print(url)

    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    json_response = response.json()
    #print(json_response)
    result_count = json_response["meta"]["result_count"]
    return result_count

def CoinGecko(token_sc):
    token_sc = token_sc.lower()
    proxies = {
        "http": "http://167.71.200.126:8447",
        "https": "http://167.71.200.126:8447"
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    URL =  "https://api.coingecko.com/api/v3/coins/list?include_platform=true"
    #print(URL)
    r = requests.get(URL, proxies=proxies, headers=headers, timeout=20)
    #print(r)
    data = r.json()
    for item in data:
        if "platforms" in item and "ethereum" in item["platforms"]:
            if item["platforms"]["ethereum"] == token_sc:
                return "Yes"
    return "No"

def cmc(token_sc,token_symbol):
    # Set the environment variable
    os.environ['CMC-Key'] = "2ec4f723-8f0a-4060-9a60-b5c2bfd9dff8"

    # Retrieve the environment variable
    api_key = os.environ.get('CMC-Key')

    symbol = token_symbol
    token_sc = token_sc

    cmc_client = coinmarketcapapi.CoinMarketCapAPI(api_key)
    
    try:
        response = cmc_client.cryptocurrency_info(symbol = symbol)
        findsc = response.data[symbol][0]['platform']['token_address']
        #print(findsc)
        if token_sc == findsc:
            data = "Yes"
            return(data)
        else:
            data = "No"
            return(data)
    except Exception as e:
        data = "No"
        return(data)

def merge_and_remove_duplicates(intermediate_file, rescan_file):
    with open(intermediate_file, 'r') as f1, open(rescan_file, 'r') as f2:
        intermediate_lines = set(f1.readlines())
        rescan_lines = set(f2.readlines())
        unique_lines = intermediate_lines - rescan_lines
    with open(rescan_file, 'a') as f2:
        for line in unique_lines:
            f2.write(line)
    with open(intermediate_file, 'w') as f1:
        f1.write('')



def do_scan():
    merge_and_remove_duplicates(DB_INTERMEDIATE, DB_RESCANTOKEN)
    #getTokenInformationBloxy("0x4cd988afbad37289baaf53c13e98e2bd46aaea8c")
    #os._exit(1)
    erc20 = w3.eth.contract(address=Web3.toChecksumAddress(exchangeFactorySC.lower()), abi=ERC20_UNISWAP_EXCHANGEFACTORY_ABI)
    exchange_count = erc20.functions.allPairsLength().call()

    #print("Print 1 ---- "+str(time.ctime()))
    dellist = []
    dbline = ReadRescanFile()
    #print(exchange_count)
    #jumlah = 0
    #last_from_db = cekLastNumberFromDB() - 1
    for LoopNumber, data in enumerate(CheckRescanToken()):
        data = int(data)
        pair_sc = w3.eth.contract(address=Web3.toChecksumAddress(exchangeFactorySC.lower()),
                                abi=ERC20_UNISWAP_EXCHANGEFACTORY_ABI)
        exchange_sc = erc20.functions.allPairs(data).call()
        exchange_sc_contract = w3.eth.contract(address=Web3.toChecksumAddress(exchange_sc.lower()),
                                               abi=UNISWAP_EXCHANGE_ABI)
        print(data)



        reserves = exchange_sc_contract.functions.getReserves().call()
        #print(reserves)

        token0 = exchange_sc_contract.functions.token0().call()
        token0_symbol = getTokenInformation(token0)['symbol']

        #print(data)
        #print("Print 2 ---- "+str(time.ctime()))

        try:
            if token0_symbol == "null":
                token0_symbol = getTokenName(token0)['symbol']
        except:
            token0_symbol = "null"
        '''
        try:
            token0_symbol = getTokenInformation(token0)['symbol']
        except:
            token0_symbol = "null"
        '''
        token1 = exchange_sc_contract.functions.token1().call()
        token1_symbol = getTokenInformation(token1)['symbol']

        #print("Print 3 ---- "+str(time.ctime()))

        try:
            if token1_symbol == "null":
                token1_symbol = getTokenName(token1)['symbol']
        except:
            token1_symbol = "null"
        ''''
        try:
            token1_symbol = getTokenInformation(token1)['symbol']
        except:
            token1_symbol = "null"
        '''



        


        #if "WETH" not in token0_symbol or "WETH" not in token1_symbol:
        #    continue
        if token0_symbol.upper() == "WETH":
            token_sc = token1
            token_symbol = token1_symbol.upper()
        elif token1_symbol.upper() == "WETH":
            token_sc = token0
            token_symbol = token0_symbol.upper()
        else:
            dellist.append(LoopNumber)
            print(dellist)
            continue

        #print("Print between 3-4 ---- "+str(time.ctime()))

        decimals = getTokenInformation(token_sc)['decimals']
        try:
            if decimals == "null":
                decimals = getTokenName(token_sc)['decimals']
        except:
            decimals = "null"
        token_name = getTokenInformation(token_sc)['name']

        print(token_name)

        #print("Print 4 ---- "+str(time.ctime()))

        #print(exchange_sc,token_sc,token_symbol,token_name,decimals)
        if decimals == "null" or token_symbol.upper() == "NULL" or token_name.upper()  == "NULL":
            token_info_bloxy =  getTokenInformationBloxy(token_sc)
            decimals = token_info_bloxy['decimals']
            token_symbol = token_info_bloxy['symbol']
            token_symbol = token_symbol.strip()
            token_symbol = token_symbol.upper()
            token_name = token_info_bloxy['name']
            token_name = token_name.strip()
            token_name = token_name.upper()
        weth_balance = getBalanceWeth(exchange_sc)
        weth_balance = format(float(weth_balance),".2f")

        #print("Print 5 ---- "+str(time.ctime()))

        '''
        if decimals == "null" or token_symbol.upper() == "NULL":
            token_info_etherscan =  getTokenInformationEtherscan(token_sc)
            decimals = token_info_etherscan['decimals']
            token_symbol = token_info_etherscan['symbol']
        '''
        '''
        if decimals != "null" or token_symbol != "NULL":
            out_tokenall = token_sc.lower()[2:]+":"+token_symbol.strip()+":"+str(decimals)
            print(out_tokenall)
        '''
        '''
        if decimals != "null" or token_symbol != "NULL":
            out_tokenall = token_symbol.strip()+":"+exchange_sc.lower()
            print(out_tokenall)
        '''
        #print("Print 6 ---- "+str(time.ctime()))
        #print(token_sc)
        #print(token_symbol)

        #print(token_sc.lower()+":"+token_symbol+":"+token_name+":"+str(weth_balance)+":"+exchange_sc.lower()+"\n")
        if decimals != "null" or token_symbol != "NULL" :
            if token_symbol == "":
                continue
            
            #print("Print 7 ---- "+str(time.ctime()))

            token_db = cekTokenDB(token_sc)
            token_exchange = cekExchangeDB(exchange_sc)

            #print("Print 8 ---- "+str(time.ctime()))
            
            if token_db == True or token_exchange == True :
                dellist.append(LoopNumber)
                print(dellist)
                continue
            
            #time.sleep(5)
            try:
                CreatorAndTrx = TokenCreatorAndTRX(token_sc)
            except:
                continue
            
            #time.sleep(5)
            #print(data)
            #print(token_sc)
            #print(CreatorAndTrx['txhash'])

            #BLKNumberAndNonce = GetTrxDetails(CreatorAndTrx['txhash'])
            time.sleep(10)
            try:
                FirstWalletBLKNumberAndNonce = getWalletReceiverAndNonce(CreatorAndTrx['txhash'])
            except IndexError as a:
                print(a)
                continue

            time.sleep(5)
            Holders = str(holdersByAPI(token_sc))
            time.sleep(5)
            
            TimeDifference = blockTimeStamp(currentBlock()) - blockTimeStamp(int(FirstWalletBLKNumberAndNonce['blocknumber']))

            #print(TimeDifference)




            humanTime = str(datetime.timedelta(seconds=TimeDifference))

            #print("Print 9 ---- "+str(time.ctime()))

            if TimeDifference > 604800:
                dellist.append(LoopNumber)
                print(TimeDifference)
                print("time difference section")
                print(dellist)
                continue

            ####
            ## CHECK WETH IF NOT ZERO
            ####
            if reserves[0] == 0:
                continue

            #print("Print 10 ---- "+str(time.ctime()))

            send(token_sc)
            honeyinfo = honeypot()

            info = "Uniswap V2 New Token\n"
            info += "symbol : "+token_symbol+"\n"
            info += "name : " + token_name + "\n"
            info += "Token SC : "+str(token_sc)+"\n"
            info += "Exchange SC : "+str(exchange_sc)+"\n"
            info += "Weth Balance: " + str(weth_balance) + " WETH \n"
            info += str(time.ctime())+"\n"
            info += "----"
            input_tokenDB = token_sc[2:].lower()+":"+token_symbol+":"+str(decimals)+"\n"
            input_exchangeDB = token_symbol+":"+exchange_sc.lower()+"\n"
            #input_datatokenDB = token_sc.lower()+":"+token_symbol+":"+token_name+":"+str(weth_balance)+":"+exchange_sc.lower()+"\n"
            input_datatokenDB = str(data)+":"+token_sc.lower()+":"+token_symbol+":"+token_name+":"+str(weth_balance)+":"+exchange_sc.lower()+":"+CreatorAndTrx['wallet']+":"+CreatorAndTrx['txhash']+":"\
                +FirstWalletBLKNumberAndNonce['receiver']+":"+str(FirstWalletBLKNumberAndNonce['blocknumber'])+":"+str(currentBlock())+":"+str(TimeDifference)+":["+str(humanTime)+"]:"+str(FirstWalletBLKNumberAndNonce['nonce'])\
                    +":"+Holders+":"+str(honeyinfo['honeypot'])+":"+str(honeyinfo['buytax'][0])+":"+str(honeyinfo['selltax'])+":"+TokenRenounce(token_sc)+":"+str(search_twitter(token_sc))+":"+CoinGecko(token_sc)+":"+cmc(token_sc, token_symbol)+"\n"
            #print(input_tokenDB)
            #print(input_exchangeDB)
            #print(info)
            #print(LoopNumber)
            print(input_datatokenDB)
            appendTokenDB(input_tokenDB)
            appendExchangeDB(input_exchangeDB)
            appendDataTokenDB(input_datatokenDB)
            
            #print(dbline)
            #print(dbline[LoopNumber])
            dellist.append(LoopNumber)
            print(dellist)
            #setLastNumberFromDB(str(data))

            kirimDiscord(URL_DISCORD,info)
            URL_CUSTOM = "https://discord.com/api/webhooks/766309225404039278/fEV940Q-t9YJVjw7vBNmKAJLZ_O6yRLhBh2_jv1S-sTDWStULLKrC_9ze1KHMo5jSKNv"
            if float(weth_balance) > float(500):
                kirimDiscord(URL_CUSTOM,info)
        #print(out_tokenall)
        time.sleep(1)
    for ele in sorted(dellist, reverse = True):
        del dbline[ele]
    #print(dbline)
    WriteRescanFile(dbline)
    #setLastNumberFromDB(str(exchange_count))

while True:
    print("start ---- "+str(time.ctime()))
    do_scan()
    print("finish ---- "+str(time.ctime()))
    time.sleep(40)
